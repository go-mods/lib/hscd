// The hscd (hash code) package offers various implementations of
// algorithms to generate hash codes from arbitrary data (based on
// https://archive.vn/KJeJy#selection-4260.0-4260.1).
package hscd

import (
	"gitlab.com/go-mods/lib/bops"
)

// The length of computed hash codes in bytes (default: 4).
var HashLen = 4

// DJB implements a slightly modified version of the 'Chris Torek
// hash algorithm' by Dan Bernstein (XOR instead of ADD).
func DJB(key []byte) []byte {
	h := make([]byte, HashLen)
	p := []byte{33}

	for i, m := 0, len(key); i < m; i += HashLen {
		ptr := key[i:]

		if len(ptr) > HashLen {
			ptr = ptr[:HashLen]
		}

		h = bops.Xor(bops.Mul(h, p), ptr)
	}

	return h
}

// SAX implements the 'shift-add-xor hash algorithm' which was
// designed as a string hashing function.
func SAX(key []byte) []byte {
	h := make([]byte, HashLen)

	for i, m := 0, len(key); i < m; i += HashLen {
		ptr := key[i:]

		if len(ptr) > HashLen {
			ptr = ptr[:HashLen]
		}

		l := bops.ShiftL(h, 5)
		r := bops.ShiftR(h, 2)
		h = bops.Xor(h, bops.Add(ptr, bops.Add(l, r)))
	}

	return h
}

// The FNV hash, short for Fowler/Noll/Vo in honor of the creators,
// is a very powerful algorithm that, not surprisingly, follows the
// same lines as Bernstein's modified hash with carefully chosen
// constants.
func FNV(key []byte) []byte {
	h := make([]byte, HashLen)
	h = bops.Or(h, []byte{0x81, 0x1C, 0x9D, 0xC5})
	p := []byte{0x01, 0x00, 0x01, 0x93}

	for i, m := 0, len(key); i < m; i += HashLen {
		ptr := key[i:]

		if len(ptr) > HashLen {
			ptr = ptr[:HashLen]
		}

		h = bops.Xor(bops.Mul(h, p), ptr)
	}

	return h
}

// OAT implements the 'One-at-a-Time hash algorithm' by Bob Jenkins.
func OAT(key []byte) []byte {
	h := make([]byte, HashLen)

	for i, m := 0, len(key); i < m; i += HashLen {
		ptr := key[i:]

		if len(ptr) > HashLen {
			ptr = ptr[:HashLen]
		}

		h = bops.Add(h, ptr)
		h = bops.Add(h, bops.ShiftL(h, 10))
		h = bops.Xor(h, bops.ShiftR(h, 6))
	}

	h = bops.Add(h, bops.ShiftL(h, 3))
	h = bops.Xor(h, bops.ShiftR(h, 11))
	h = bops.Add(h, bops.ShiftL(h, 15))

	return h
}

// JSW implements a hash algorithm that uses a table of 256 random
// generated numbers to reach a good distribution and performance
// (by Julienne Walker).
func JSW(key []byte) []byte {
	h := make([]byte, HashLen)
	h = bops.Or(h, []byte{0x01, 0x00, 0x01, 0x4F})

	for i, m := 0, len(key); i < m; i++ {
		h = bops.Xor(bops.Or(bops.ShiftL(h, 1), bops.ShiftR(h, 31)), randoms[key[i]])
	}

	return h
}

var randoms = [][]byte{
	[]byte{31}, []byte{219}, []byte{181}, []byte{102}, []byte{227}, []byte{42}, []byte{103}, []byte{180}, []byte{124}, []byte{155}, []byte{43}, []byte{221}, []byte{7}, []byte{160}, []byte{21}, []byte{64}, []byte{255}, []byte{134}, []byte{203}, []byte{168}, []byte{229}, []byte{133}, []byte{191}, []byte{10}, []byte{48}, []byte{202}, []byte{146}, []byte{209}, []byte{210}, []byte{87}, []byte{4}, []byte{163},
	[]byte{250}, []byte{70}, []byte{217}, []byte{28}, []byte{121}, []byte{188}, []byte{51}, []byte{182}, []byte{139}, []byte{187}, []byte{11}, []byte{33}, []byte{237}, []byte{118}, []byte{147}, []byte{236}, []byte{140}, []byte{177}, []byte{14}, []byte{12}, []byte{37}, []byte{6}, []byte{50}, []byte{39}, []byte{108}, []byte{195}, []byte{0}, []byte{207}, []byte{100}, []byte{129}, []byte{240},
	[]byte{82}, []byte{245}, []byte{72}, []byte{222}, []byte{63}, []byte{176}, []byte{105}, []byte{172}, []byte{123}, []byte{13}, []byte{141}, []byte{232}, []byte{101}, []byte{220}, []byte{193}, []byte{19}, []byte{161}, []byte{25}, []byte{126}, []byte{23}, []byte{179}, []byte{96}, []byte{90}, []byte{244}, []byte{26}, []byte{175}, []byte{231}, []byte{130}, []byte{224}, []byte{116}, []byte{228}, []byte{166}, []byte{117},
	[]byte{8}, []byte{61}, []byte{212}, []byte{184}, []byte{68}, []byte{192}, []byte{127}, []byte{41}, []byte{91}, []byte{54}, []byte{164}, []byte{249}, []byte{115}, []byte{84}, []byte{170}, []byte{153}, []byte{152}, []byte{93}, []byte{78}, []byte{62}, []byte{194}, []byte{167}, []byte{201}, []byte{162}, []byte{32}, []byte{144}, []byte{150}, []byte{69}, []byte{223}, []byte{135}, []byte{27},
	[]byte{76}, []byte{60}, []byte{186}, []byte{86}, []byte{88}, []byte{247}, []byte{22}, []byte{45}, []byte{1}, []byte{151}, []byte{104}, []byte{156}, []byte{226}, []byte{120}, []byte{122}, []byte{80}, []byte{148}, []byte{29}, []byte{204}, []byte{44}, []byte{112}, []byte{213}, []byte{216}, []byte{66}, []byte{97}, []byte{242}, []byte{83}, []byte{137}, []byte{49}, []byte{248}, []byte{20}, []byte{16}, []byte{110},
	[]byte{211}, []byte{190}, []byte{198}, []byte{243}, []byte{81}, []byte{234}, []byte{230}, []byte{67}, []byte{9}, []byte{254}, []byte{252}, []byte{55}, []byte{241}, []byte{58}, []byte{119}, []byte{17}, []byte{159}, []byte{99}, []byte{59}, []byte{109}, []byte{74}, []byte{214}, []byte{94}, []byte{239}, []byte{149}, []byte{114}, []byte{200}, []byte{30}, []byte{106}, []byte{107}, []byte{253}, []byte{73}, []byte{185},
	[]byte{173}, []byte{65}, []byte{98}, []byte{3}, []byte{89}, []byte{56}, []byte{131}, []byte{136}, []byte{157}, []byte{36}, []byte{251}, []byte{85}, []byte{2}, []byte{95}, []byte{196}, []byte{235}, []byte{34}, []byte{205}, []byte{165}, []byte{197}, []byte{233}, []byte{47}, []byte{77}, []byte{169}, []byte{178}, []byte{225}, []byte{132}, []byte{125}, []byte{18}, []byte{238}, []byte{15}, []byte{111},
	[]byte{142}, []byte{171}, []byte{38}, []byte{183}, []byte{52}, []byte{92}, []byte{40}, []byte{57}, []byte{46}, []byte{24}, []byte{138}, []byte{53}, []byte{79}, []byte{128}, []byte{113}, []byte{189}, []byte{208}, []byte{5}, []byte{246}, []byte{71}, []byte{35}, []byte{218}, []byte{206}, []byte{143}, []byte{199}, []byte{154}, []byte{215}, []byte{145}, []byte{158}, []byte{174}, []byte{42},
}

// ELF implements a hash algorithm that in addition of shifting
// makes use of the AND and NOT operators.
func ELF(key []byte) []byte {
	var g []byte
	h := make([]byte, HashLen)
	a := []byte{0xF0, 0x00, 0x00, 0x00}

	for i, m := 0, len(key); i < m; i += HashLen {
		ptr := key[i:]

		if len(ptr) > HashLen {
			ptr = ptr[:HashLen]
		}

		h = bops.Add(bops.ShiftL(h, 4), ptr)
		g = bops.And(h, a)

		if !isZero(g) {
			h = bops.Xor(h, bops.ShiftR(g, 24))
		}

		h = bops.And(h, bops.Not(g))
	}

	return h
}

func isZero(data []byte) bool {
	for i, n := 0, len(data); i < n; i++ {
		if data[i] != 0 {
			return false
		}
	}

	return true
}

// JEN implements the 'Jenkins hash algorithm' by Bob Jenkins.
func JEN(key, init []byte) []byte {
	a, b, c := []byte{0x9E, 0x37, 0x79, 0xB9}, []byte{0x9E, 0x37, 0x79, 0xB9}, init
	len := len(key)

	for len >= 12 {
		a = bops.Add(a, bops.Add(key[0:1], []byte{0x00, 0x00, key[1], 0x00}))
		a = bops.Add(a, []byte{0x00, key[2], 0x00, 0x00})
		a = bops.Add(a, []byte{key[3], 0x00, 0x00, 0x00})
		b = bops.Add(b, bops.Add(key[4:5], []byte{0x00, 0x00, key[5], 0x00}))
		b = bops.Add(b, []byte{0x00, key[6], 0x00, 0x00})
		b = bops.Add(b, []byte{key[7], 0x00, 0x00, 0x00})
		c = bops.Add(c, bops.Add(key[8:9], []byte{0x00, 0x00, key[9], 0x00}))
		c = bops.Add(c, []byte{0x00, key[10], 0x00, 0x00})
		c = bops.Add(c, []byte{key[11], 0x00, 0x00, 0x00})

		a, b, c = mix(a, b, c)

		key = key[12:]
		len -= 12
	}

	c = bops.Add(c, []byte{byte(HashLen >> 24), byte(HashLen >> 16), byte(HashLen >> 8), byte(HashLen)})

	switch len {
	case 11:
		c = bops.Add(c, []byte{key[10], 0x00, 0x00, 0x00})
		fallthrough
	case 10:
		c = bops.Add(c, []byte{0x00, key[9], 0x00, 0x00})
		fallthrough
	case 9:
		c = bops.Add(c, []byte{0x00, 0x00, key[8], 0x00})
		fallthrough
	case 8:
		b = bops.Add(b, []byte{key[7], 0x00, 0x00, 0x00})
		fallthrough
	case 7:
		b = bops.Add(b, []byte{0x00, key[6], 0x00, 0x00})
		fallthrough
	case 6:
		b = bops.Add(b, []byte{0x00, 0x00, key[5], 0x00})
		fallthrough
	case 5:
		b = bops.Add(b, []byte{0x00, 0x00, 0x00, key[4]})
		fallthrough
	case 4:
		a = bops.Add(a, []byte{key[3], 0x00, 0x00, 0x00})
		fallthrough
	case 3:
		a = bops.Add(a, []byte{0x00, key[2], 0x00, 0x00})
		fallthrough
	case 2:
		a = bops.Add(a, []byte{0x00, 0x00, key[1], 0x00})
		fallthrough
	case 1:
		a = bops.Add(a, []byte{0x00, 0x00, 0x00, key[0]})
	}

	_, _, c = mix(a, b, c)

	return c
}

func mix(a, b, c []byte) ([]byte, []byte, []byte) {
	a = bops.Sub(a, b)
	a = bops.Sub(a, c)
	a = bops.Xor(a, bops.ShiftR(c, 13))
	b = bops.Sub(b, c)
	b = bops.Sub(b, a)
	b = bops.Xor(b, bops.ShiftL(a, 8))
	c = bops.Sub(c, a)
	c = bops.Sub(c, b)
	c = bops.Xor(c, bops.ShiftR(b, 13))
	a = bops.Sub(a, b)
	a = bops.Sub(a, c)
	a = bops.Xor(a, bops.ShiftR(c, 12))
	b = bops.Sub(b, c)
	b = bops.Sub(b, a)
	b = bops.Xor(b, bops.ShiftL(a, 16))
	c = bops.Sub(c, a)
	c = bops.Sub(c, b)
	c = bops.Xor(c, bops.ShiftR(b, 5))
	a = bops.Sub(a, b)
	a = bops.Sub(a, c)
	a = bops.Xor(a, bops.ShiftR(c, 3))
	b = bops.Sub(b, c)
	b = bops.Sub(b, a)
	b = bops.Xor(b, bops.ShiftL(a, 10))
	c = bops.Sub(c, a)
	c = bops.Sub(c, b)
	c = bops.Xor(c, bops.ShiftR(b, 15))
	return a, b, c
}
