package hscd

import (
	"fmt"
	"testing"

	"gitlab.com/go-mods/lib/bops"
)

type params struct {
	name  string
	count int
}

const param_count = 500
const param_factor = 5000

func BenchmarkDJB(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkDJB(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkSAX(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkSAX(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkFNV(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkFNV(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkOAT(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkOAT(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkJSW(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkJSW(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkELF(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkELF(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func BenchmarkJEN(b *testing.B) {
	benchmarks := [param_count]params{}
	hashMap := make(map[string]string)

	for i := 0; i < param_count; i++ {
		x := (i + 1) * param_factor
		benchmarks[i] = params{fmt.Sprintf("%d", x), x}
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			c := 0.0

			for i := 0; i < b.N; i++ {
				c = benchmarkJEN(bm.count, hashMap, b)
			}

			// if b.N == 1 {
			// 	// Suppress metrik with '-benchtime 1x'. Side effect: if the function
			// 	// took longer than benchtime b.N will never become greater than 1
			// 	// (i.e the reported metric might not be uniform).
			// 	b.ReportMetric(0, "ns/op")
			// }

			b.ReportMetric(c, "collisions")
		})
	}
}

func benchmarkDJB(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := DJB([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkSAX(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := SAX([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkFNV(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := FNV([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkOAT(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := OAT([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkJSW(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := JSW([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkELF(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := ELF([]byte(key))

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}

func benchmarkJEN(N int, hashMap map[string]string, b *testing.B) float64 {
	collisions := 0
	init := []byte{0x13, 0x12, 0xA1, 0x01}

	for n := 0; n < N; n++ {
		key := fmt.Sprintf("somekey%d", n)
		hsh := JEN([]byte(key), init)

		if prev, contains := hashMap[bops.Str(hsh)]; contains && prev != key {
			collisions++
		} else {
			hashMap[bops.Str(hsh)] = key
		}
	}

	return float64(collisions)
}
